This document provides a guide to compile and run the GROMACS tests as we did in the hackathon. 


COMPILATION

Get into the directory of the source and create a “build” folder.
Run the “config-gromacs.sh” script from the taball.
Do “make –j N” where N is the number of cores in the node.
Do “make check” to run unit tests, pass -DREGRESSIONTEST_DOWNLOAD=ON to download
and run the end-to-end regression-test suite as well.
For further details, look at the well-documented "config-gromacs.sh" script. ;)

Machine-specific notes:
    - At the time of assessing the TX2 performance, with both gcc 7.2 and armclang 1.4
    the -mcpu=thunderx2t99 compiler flag seemed to give a marginal improvement. It is
    however recommended to revise the use of the flag with new compiler releases (as
    on other platforms it often has no or negative effect).
    - On most x86 platforms -mtune or -march flags have little to no effect.
    - In order to run with >64 OpenMP threads/process, the default bitmask size for
    sparse reduction masks need to be increased from 64 to 128 (or any multiple of 64).
    This can be done using the -DGMX_OPENMP_MAX_THREADS=128 option.

RUN

We used two test cases in the Hackathon:
    - A smaller problem, in the “rnase_cubic” folder
    - A larger problem, in the “ion_channel_vsites.bench

In the top-level benchmarks directory there is a bench-wrapper.sh script which can be used to
launch the "runbench.sh" benchmark driver.

The following needs to be specified before running:

Lines 24-26: Define the features of the machine:
    hw_tpc: Threads per core (e.g. 2 for Intel Hyper-Threaded chip)
    cores_per_cpu: Number of physical cores of the chip
    threads_per_cpu: hw_tpc * cores_per_cpu


Line 191: gmx_gcc_dir*: paths to the gromacs binaries can be defined (note: without the gmx binary itself).
Add more items to this list to test different binaries.

Lines 231-244: Each “X,Y,Z” item in the list represents a core/thread configuration to be tested, where:
    X: # of (thread)MPI ranks
    Y: # of OMP threads
    Z: # pinning stride.
The pinning stride is the thread affinity stride over the list of hardware threads in a contiguous by-core indexing
(i.e. with 2-way SMT 0,1,2,3 are the two hardware threads of the first and second core, resp.).
The configures list can also be passed using the LAUNCH_CONFIG_LIST env var.

Lines 253-260: Each entry of the list represents a build to be tested, from the ones defined in line 191.
Each item of this list is composed of “X,Y,Z” where:
    X: a path to a build configured in line 191-… 
    Y: suffix that will used in the Logs to identify the run
    Z: module that will be loaded to run the binary

Once “runbench.sh” is properly configured, execute it one-by-one in the target benchmark directores 
(pme-bench and if desired rf-bench for each input case) or use the bench-wrapper.sh to launch the
all benchmarks at once (see the comments in the script for more details).

Notes:
- make sure that in each target bench directory there is a “topol.tpr” file, this is normally a
symbolic link to the “pme.tpr” (or "rf.tpr") file in the parent directory. If not, create it.
- fixed iteration count runs are convenient to compare and to run quickly, but step counts need
to be picked for each system (to avoid overly short runs); this can be done using NSTEPS and RESETSTEP;
alternatively, by the TIME_LIMITED_BENCH env var will trigger a fixed time length runs, by default
1 minute each (can be changed using the MAX_HOURS_LIMIT env var)


The runs will generate a logfile for each of the configurations defined in “runbench.sh” with the form
“bench_XXC_YYxZZT_QQTPC_TAG_cpu.log”, where:
    XX: number of cores used
    YY: number of thread-MPI ranks
    ZZ: number of OpenMP threads per rank
    QQ: number of threads per core

The application-specific performance number (ns/day) as well as wall-time are at the end of each log file on
the line starting with “Performance:”

