#!/bin/bash
#
# This script has the sole purpose to launch the benchmark runs using the
# runbench.sh script for the ion_channel_vsites and rnase_cubic bench sets.

runbench=./runbench.sh
RUN_DIRECTORIES="rnase_cubic/pme-runs rnase_cubic/rf-runs  ion_channel_vsites.bench/pme-runs ion_channel_vsites.bench/rf-runs"

# trigger TIME limited benchmarks
export TIME_LIMITED_BENCH=1
#export MAX_HOURS_LIMIT=0.002

# repeat in run twice
export REPEAT_BENCH=2

# use tabulated kernels on the TX2
export GMX_NBNXN_EWALD_TABLE=1

#export LAUNCH_CONFIG_LIST="1,4,2 1,8,1"

# If testing a single binary/build that can be passed here, fmt: PATH_TO_GMX,suffix,MODULE_TO_LOAD
# export GMX_VERSIONS_OVERRIDE=
#export GMX_VERSIONS_OVERRIDE="~/projects/gromacs/gromacs-master/build_gcc52_cuda90/bin,gcc52,gcc/5.2"

runbench=$(readlink -f $runbench)
for dir in $RUN_DIRECTORIES; do
[[ ! -d $dir ]] && { echo -e "\n\n ERROR: the directory \"$dir\" does not exits, skipping it\n\n"; continue; }

(
  cd $dir
  exec $runbench
  cd - 1>&- 2>&-
)
done
