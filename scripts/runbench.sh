#!/bin/bash

# Can't use set -e in prduction as it would abot the script if
# e.g. an incompatible mdrun launch config is executed which we do not handle
# ATM.
#set -e

# $@ error message
function err_exit 
{
  echo "error: $@"
  exit 1
}

export GMX_NO_CREDITS=1
export GMX_MAXBACKUP=1
export GMX_MAXBACKUP=-1
#export SORT_LOGS_BY_THREADS_USED=1
export SORT_LOGS_BY_CORES_USED=1

### FOR TX2 we use tabulated non-bonded kernels
[[ "$HOSTNAME" =~ gw4arm ]] && { export GMX_NBNXN_EWALD_TABLE=1; }

###############################################################################
## IMPORTANT Machine defaults
## NOTE that if these are not correct, log file naming will be messed up
threads_per_cpu=128
cores_per_cpu=32
hw_tpc=4 # threads pr CPU, i.e. SMT
hw_tpc=${HW_TPC:-$hw_tpc}

[[ $((cores_per_cpu*hw_tpc)) -ne $threads_per_cpu ]] && { err_exit "core/thread count mismatch: $cores_per_cpu*$hw_tpc != $threads_per_cpu"; }

# Sanity check: the /proc/cpuinfo "processor" count should match the total?
total_threads_per_node=$(grep '^processor' /proc/cpuinfo | wc -l)
if [ $((total_threads_per_node % hw_tpc)) -ne 0 ]; then
  echo
  echo "     WARNING: the number of processors in /proc/cpuinfo is not divisible by TPC=$hw_tpc" 
  echo
fi
###############################################################################

###############################################################################
## Some mdrun default options below, configurable with env vars
nst=10000
#nst=4000
nst=${NSTEPS:-$nst}

resetstep=0
resetstep=${RESETSTEP:-$resetstep}

# 1 minute default
max_hours_limit=0.0167
max_hours_limit=${MAX_HOURS_LIMIT:-$max_hours_limit}

tpr=topol.tpr
tpr=${TPR:-$tpr}

nstl=A
nstl=${NSTLIST:-$nstl}

NB=cpu
nb=${NB:-$nb}

global_suffix=$nb
#global_suffix=gpu

repeat_bench=1
repeat_bench=${REPEAT_BENCH:-$repeat_bench}

global_suffix=${SUFFIX:-$global_suffix}
[ -n "$global_suffix" ] && global_suffix="_${global_suffix}"
###############################################################################


###############################################################################
## assemble the default options string
# we either run time-limited bench with -maxh/-resethway OR fixed step with
# -nsteps/-resetstep; the latter is great for comparing logs side-by-side,
# but requires picking a sensible step count for each input/machine/core count.
if [ -n "$TIME_LIMITED_BENCH" ]; then
  run_length_opt="-nsteps -1 -maxh $max_hours_limit -resethway"
else
  if [ $resetstep -eq 0 ]; then
    run_length_opt="-nsteps $nst -resethway"
  else
    run_length_opt="-nsteps $nst -resetstep $resetstep"
  fi
fi
opts="-v -quiet -noconfout -npme 0 -nb $nb -pin on"
[[ "$nstl" != "A" || "$nstl" != "A" ]] && opts="$opts -nstlist $nstl"

[ -z "$NOTUNE" ] && tunepme="-notunepme"

###############################################################################

# $1 - np 
# $2 - nt
# $3 - stride 
# $4 - nsteps XXX unused 
# $5 - tpr
# $6 - mdrun 
# $7 - log suffix
# $8 - more mdrun cmdline options
function run_bench_main
{
  local np=$1
  local nt=$2
  local stride=$3
  #local nsteps=$4
  local tpr=$5
  local mdrun=$6
  local versuffix=$7
  local xopt=$8
 
  tpc=$((hw_tpc/stride))
  if [ -z "$DISABLE_TPC_SUFFIX" ]; then
    [[ $((tpc*stride)) -ne $hw_tpc ]] && { echo "WARNING: TPC/stride mismatch: hw_tpc=$hw_tpc, stride=$stride"; }
    local tpc_suffix="_${tpc}TPC"
  fi

  local ncores_tot=$((np*nt/tpc))
  local nthreads_tot=$((np*nt))
  if [ -n "$SORT_LOGS_BY_CORES_USED"   ]; then
    prefix_str="_${ncores_tot}C"
  elif [ -n "$SORT_LOGS_BY_THREADS_USED" ]; then
    prefix_str="_${nthreads_tot}T"
  fi

  stride_opt="-pinstride $stride"
  runopts="$opts $run_length_opt"

  ##
  ## WARNING: workaround for 28C chips
  ## 
  if [[ "$cores_per_cpu" -eq 28 ]] ; then
    [ $np -eq 7 ] && runopts="$runopts -dd 7 1 1"
    [ $np -eq 14 ] && runopts="$runopts -dd 7 2 1"
    [ $np -eq 28 ] && runopts="$runopts -dd 7 4 1"
  fi
  runopts="$runopts $tunepme"

  subcmd=mdrun

  for i in $(seq $repeat_bench); do
    [ "$repeat_bench" -gt 1 ] && repeat_suffix=_${i}
    logf=bench${prefix_str}_${np}x${nt}T${tpc_suffix}_${versuffix}${repeat_suffix}${XSUFF}

    echo Running "\
      $mdrun $subcmd -ntmpi $np -ntomp $nt -g $logf -s $tpr $runopts $stride_opt $xopt $XOPT"
    if [ -z "$DRY" ]; then 
      $mdrun $subcmd -ntmpi $np -ntomp $nt -g $logf -s $tpr $runopts $stride_opt $xopt $XOPT
    else
      echo "DRY run!"
    fi
  done
}

# Extracts the suffix of the gmx or mdrun binary passed and returns it prefixed with the
# separator charater "_".
# $1 - binary
function extract_bin_suffix
{
  local SEPARATOR="_"
  local binsuffix=$(echo $1 | sed -E -e 's/(gmx|gmx_mpi|mdrun|mdrun_mpi)[.\-\_]//')
  [ -n "$binsuffix" ] && echo _${binsuffix}
}

# Appends the suffix of the binary passed to the string passed as second paramater.
# $1 - binary
# $2 - suffix to append to
function append_bin_suffix 
{
  local binsuffix=$(extract_bin_suffix $1)
  [ -n i"$binsuffix" ] && echo $2${binsuffix} || echo $2
}

# Sets up the environment with the requested software
# This typically means loading the module with the name passed, but any custom
# logic can be implemented here.
# $1 - name of software to load 
function load_software
{
  local _sw_to_load=$1

  # custom cases can go here (e.g. when paths need to be set up manually
  if [ -n "$_sw_to_load" ]; then 
    echo "---> loading module(s) $load"
    module load $load
  fi
}

###############################################################################
# The GROMACS binary locations and list of binaries to run with

gmx_root=/home/brx-pszilard/gromacs-17dev

gmx_gcc_dir=${gmx_root}/build_gcc72_fftw-cray336/bin/
gmx_gcc_dir_opt=${gmx_root}/build_gcc72_fftw-cray336_OPT/bin/
gmx_gcc_dir_opt2=${gmx_root}/build_gcc72_fftw-cray336_OPT2/bin/
gmx_clang_dir=${gmx_root}/build_armcc14_fftw-cray336/bin/
gmx_clang_dir_opt=${gmx_root}/build_armcc14_fftw-cray336_OPT/bin/
gmx_clang_dir_opt2=${gmx_root}/build_armcc14_fftw-cray336_OPT2/bin/

# List of binaries to loop through when testing;
# useful for testing mulitple build in the same tree (e.g. optimization flags)
bins_list_default="gmx"
#bins_list="gmx.omp-max-th128"
bins_list=${BINS_LIST:-$bins_list_default}
###############################################################################


###############################################################################
# launch_config_list is a list of gmx mdrun launch configs in the following format:
#   "#RANKS,#OMP_THREADS,STRIDE"
# where STRIDE is the pinning stride passed to mdrun (i.e. when 2-way SMT
# is enabled pass 1 to use all hw threads and pass 2 to put 1 threads/core.
###############################################################################
if [ -n "$LAUNCH_CONFIG_LIST" ]; then
  launch_config_list=( $LAUNCH_CONFIG_LIST )
  #IFS=', ' read -r -a launch_config_list <<< "$LAUNCH_CONFIG_LIST"
###############################################################################
###############################################################################
## the configs for the Isambard TX2 EA 28C/socket nodes are at the bottom of the script
elif [ -n "$HSOCKET_OMP_ONLY" ]; then 
  launch_config_list=( 
                # 16 C 
                "1,16,4"  "1,32,2"  "1,64,1" 
              )
elif [ -n "$OMP_ONLY" ]; then 
  launch_config_list=( \
                # 16 C 
                "1,16,4"  "1,32,2"  "1,64,1" 
                # 16 C 
                "1,32,4"  "1,64,2"  "1,128,1" 
              )
else
  launch_config_list=(  
                # 16 C 
                  "1,16,4"  "1,32,2"  "1,64,1" \
                  "8,2,4"   "8,4,2"   "8,8,1"  \
                  "16,1,4"  "16,2,2"  "16,4,1" \
                            "32,1,2"  "32,2,1" \
                                      "64,1,1" \
                # 32 C 
                  "1,32,4"  "1,64,2" "1,128,1"\
                  "8,4,4"   "8,8,2"  "8,16,1" \
                  "16,2,4"  "16,4,2"  "16,8,1" \
                            "32,2,2"  "32,4,1" \
                            "64,1,2"  "64,2,1" \
                                      "128,1,1" \
            )
###############################################################################
fi

# The GMX_VERSIONS_OVERRIDE allows passing a single GMX binary/suffix/module-load combo from "outside"
if [ -n "$GMX_VERSIONS_OVERRIDE" ]; then
  gmx_versions=( "$GMX_VERSIONS_OVERRIDE" )
else
  gmx_versions=( 
  #  "${gmx_gcc_dir},gcc72,gcc/7.2.0" \  
    "${gmx_gcc_dir_opt},gcc72_opt1,gcc/7.2.0" \  
  #  "${gmx_gcc_dir_opt2},gcc72_opt2,gcc/7.2.0" \  
  #  "${gmx_clang_dir},armhpc14,arm/hpc-compiler/1.4" \  
    "${gmx_clang_dir_opt},armhpc14_opt1,arm/hpc-compiler/1.4" \  
  #  "${gmx_clang_dir_opt2},armhpc14_opt2,arm/hpc-compiler/1.4" \  
    )
fi

echo "==> gmx_versions: ${gmx_versions[@]} "
echo "==> bins: ${bins_list}"
echo "==> launch_config_list: ${launch_config_list[@]} "

# Loop over the gmx builds in gmx_versions and benchmark each (for every binary supplied in $bins_list)
for gmx_ver in ${gmx_versions[@]}; do

  [ -n "$DEBUG" ] && echo "gmx_ver=$gmx_ver"

  IFS=',' read gmxdir suff load <<< "$gmx_ver"

  (
  load_software "$load"

  for bin in ${bins_list}; do
    gmx=${gmxdir}/${bin}
    binsuff=$(echo $bin | sed 's/gmx//')
    suffix="${suff}${binsuff}${global_suffix}"
    for conf in ${launch_config_list[@]}; do
      IFS=',' read nr nt stride <<< "$conf"
      run_bench_main ${nr} ${nt} ${stride} ${nst} ${tpr} $gmx $suffix
      
      # cooldown sleep between runs?
      [ -n "$SLEEP" ] && sleep $SLEEP
    done
  done
  )
done



exit 0
# CONFIGS for 28C TX2 nodes
elif [ -n "$HSOCKET_OMP_ONLY" ]; then 
  launch_config_list=( 
                # 14 C 
                "1,14,4"  "1,28,2"  "1,56,1" 
              )
elif [ -n "$OMP_ONLY" ]; then 
  launch_config_list=( \
                # 14 C 
                "1,14,4"  "1,28,2"  "1,56,1" 
                # 14 C 
                "1,28,4"  "1,56,2"  "1,112,1" 
              )
else
  launch_config_list=(  
                # 14 C 
                  "1,14,4"  "1,28,2"  "1,56,1" \
                  "7,2,4"   "7,4,2"   "7,8,1"  \
                  "14,1,4"  "14,2,2"  "14,4,1" \
                            "28,1,2"  "28,2,1" \
                                      "56,1,1" \
                # 28 C 
                  "1,28,4"  "1,56,2" "1,112,1"\
                  "7,4,4"   "7,8,2"  "7,16,1" \
                  "14,2,4"  "14,4,2"  "14,8,1" \
                            "28,2,2"  "28,4,1" \
                            "56,1,2"  "56,2,1" \
                                      "112,1,1" \
            )

