#!/bin/bash
#
# @author Szilárd Páll (pall.szilard@gmail.com)

##############################################################################
# Notes:
# - the CMAKE_EXTRA_OPT env var can be used to pass additional cmake command 
#   line options;
# - to trigger running the regressiontests with "make check" use:
#   + -DREGRESSIONTEST_DOWNLOAD=ON to auto-download the regression-test suite 
#     into the build tree
#   + download/clone the regressiontest suite and pass it to cmake in 
#     -DREGRESSIONTEST_PATH=<PATH>
##############################################################################

export HEADEFIX_NOTNEEDED=1

# Change to ON to use MPI
MPI_OPT=OFF
# Change to ON to use GPUs in the build (needs CUDA or OpenCL)
GPU_OPT=OFF
# Change to a SIMD-specifier string if auto-detection does not work
# or is not desired (e.g. if the node we're compiling on has different
# SIMD capabilities than the one we'll run on).
SIMD_OPT=AUTO

# Pass additional compiles flags via the CXX_FLAGS variable
# NOTE: we still have a limited amount of C files (the deprecated Group kernels
# and the TNG library), but we won't bother with these
[ -n "${CXX_FLAGS}" ] && CXX_FLAGS="-DCMAKE_CXX_FLAGS=${CXX_FLAGS}"

if [ -z "$FFTW_DIR" ]; then echo "error: FFTW_DIR env var missing"; exit 1; fi
[ -n "${STATIC_FFTW}" ] && STATIC_LIBS=OFF || STATIC_LIBS=ON

# The "header fix" typically means using a header that has the gcc version check
# disabled in order to be able to compile with newer gcc not supported by CUDA
# (without having to use a different host-compiler -- which is the other option).
[[ -z "$HEADEFIX_NOTNEEDED" && $(cc --version | head -n1) =~ " 5." ]] && \
  NVCC_FLAGS="-I${HOME}/${CUDA_HEADER_FIX_DIR}" && echo "~~~> Using CUDA header hack for CUDA $CRAY_CUDATOOLKIT_VERSION <~~~"
[ -n "$NVCC_FLAGS" ] && NVCC_FLAGS="-DCUDA_NVCC_FLAGS_RELEASE=${NVCC_FLAGS}"

if [ -z "$CC" -o -z "$CXX" ]; then
  echo "ERROR: you need to define CC and CXX env vars"
  exit 1
fi

# stupidly enough on crays FFTW_DIR points to the lib directory so we have to compensate for that
fftw_dir_tweaked="${FFTW_DIR};${FFTW_DIR}/.."

# Configure invocation non-standard options:
# - GMX_PREFER_STATIC_LIBS=ON: pick up static versions of external lib dependencies if available
# - BUILD_SHARED_LIBS=OFF: libgromacs will be linked statically, this allows to easily move
#   binaries around or build multiple binaries and suffix them in the build tree
# - GMX_CYCLE_SUBCOUNTERS=ON: turns on finer-grained cycle counters to get brake-down timings for
#   most kernels instead of just the aggregates in the main perf. table.
# - GMX_EXTERNAL_BLAS/LAPACK=OFF: don't bother detecting external BLAS/LAPACK as the internal
#   version is more than enough (not perf sensitive, mdrun does not use either).
echo "==> Using CC=$CC CXX=$CXX "
echo "==> Executing: "
echo "cmake ../ "
echo "  -DGMX_SIMD=${SIMD_OPT}  -DGMX_MPI=${MPI_OPT} -DGMX_GPU=${GPU_OPT} "
echo "  -DCMAKE_PREFIX_PATH=${fftw_dir_tweaked} "
echo "  -DGMX_PREFER_STATIC_LIBS=${STATIC_LIBS} -DBUILD_SHARED_LIBS=OFF -DGMX_BUILD_MDRUN_ONLY=OFF"
echo "  -DGMX_CYCLE_SUBCOUNTERS=ON -DGMX_EXTERNAL_BLAS=OFF -DGMX_EXTERNAL_LAPACK=OFF"
echo "  ${CXX_FLAGS} ${NVCC_FLAGS} ${CMAKE_EXTRA_OPT}"

cmake ../ \
  -DGMX_SIMD=${SIMD_OPT} -DGMX_MPI=${MPI_OPT} -DGMX_GPU=${GPU_OPT} \
  -DCMAKE_PREFIX_PATH=${fftw_dir_tweaked} \
  -DGMX_PREFER_STATIC_LIBS=${STATIC_LIBS} -DBUILD_SHARED_LIBS=OFF -DGMX_BUILD_MDRUN_ONLY=OFF \
  -DGMX_CYCLE_SUBCOUNTERS=ON -DGMX_EXTERNAL_BLAS=OFF -DGMX_EXTERNAL_LAPACK=OFF \
  ${CXX_FLAGS} ${NVCC_FLAGS} ${CMAKE_EXTRA_OPT}

# For dynamic linking on Crays CRAYPE_LINK_TYPE may be needed.
# pass BUILD=N env var to start compiling with this script ;)
if [ -z "$BUILD" ]; then
echo
#echo "==== Use: ====================="
#echo "export CRAYPE_LINK_TYPE=dynamic"
#echo "==============================="
else
#  export CRAYPE_LINK_TYPE=dynamic
  make -j$BUILD
fi
